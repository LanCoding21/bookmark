import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:bookmanager/src/model/bookmark.dart';
import 'package:bookmanager/src/bm_form/form_component.dart';
import 'package:bookmanager/src/services/bookmark_service.dart';

@Component(
    selector: 'bm-scaffold',
    templateUrl: 'bm_component.html',
    directives: [coreDirectives, BookmarkFormComponent],
    providers: [ClassProvider(BookmarkService)])
class BookmarkScaffoldComponent implements OnInit {
  BookmarkScaffoldComponent(this._bmService);

  final BookmarkService _bmService;
  List bookmarks = [];
  bool isLoading = true;

  Bookmark editedBookmark;

  @override
  Future<Null> ngOnInit() async {
    bookmarks = await _bmService.getBookmarks();
    isLoading = false;

  }

  addBookmark() {
    bookmarks.add(Bookmark());
  }

  editBookmark(int index) {
    bookmarks[index].edit = true;
  }

  removeBookmark(int index) async {
    await _bmService.removeBookmark(bookmarks[index]);
    bookmarks.removeAt(index);
  }

  updateBookmark(int index) async{
    var bm = bookmarks[index];
    if(bm.id == null){
    var resId = await _bmService.addBookmark(bm);
    bm.id = resId;
    }else {
      await _bmService.updateBookmark(bm);
    }
  }
}
