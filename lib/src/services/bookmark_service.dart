import 'dart:async';
import 'package:angular/angular.dart';
import 'package:bookmanager/src/model/bookmark.dart';
import 'package:firebase/firebase.dart';
import 'dart:html';

@Injectable()
class BookmarkService {
  BookmarkService() {
    initializeApp(
        apiKey: "AIzaSyBfEVqMyEZbmX_aRbGrItpNaQXerSEjfJ0",
        authDomain: "bookmark-manager-324d0.firebaseapp.com",
        databaseURL: "https://bookmark-manager-324d0.firebaseio.com",
        projectId: "bookmark-manager-324d0",
        storageBucket: "bookmark-manager-324d0.appspot.com",
        messagingSenderId: "264389119638",
        appId: "1:264389119638:web:32c4eaf2a3b70925effcb7",
        measurementId: "G-DDZYPDZTCT");

    _db = database();
    _ref = _db.ref('bookmarks');
  }

  Database _db;
  DatabaseReference _ref;

  Future<List<Bookmark>> getBookmarks() async {
    final List<Bookmark> bookmarks = [];
    final queryEvent = await _ref.once('value');
    final DataSnapshot snapshot = queryEvent.snapshot;
    final bmData = snapshot.val();

    bmData.forEach((key, val) {
      var details = val as Map<String, dynamic>;
      details['id']=key;
      bookmarks.add(Bookmark.fromMap(details));
    });
    return bookmarks;
  }

  Future addBookmark(Bookmark bm) async {
    var res = await _ref.push(bm.asMap());
    return res.key;
  }

  Future updateBookmark(Bookmark bm) async {
    return await _ref.child(bm.id).set(bm.asMap());
  }

  Future removeBookmark(Bookmark bm) async {
    return await _ref.child(bm.id).remove();
  }

  // Future<List<Bookmark>> getBookmarks() {
  //   return Future.delayed(Duration(seconds: 1), () {
  //     return [
  //       Bookmark(
  //         title: 'Flutter Programming Book',
  //         description: 'Belajar bahasa programing flutter',
  //         url: 'https://flutter.io',
  //         edit: false,
  //         isFresh: false,
  //       ),
  //       Bookmark(
  //         title: 'Dart Programming Book',
  //         description: 'Belajar bahasa programing flutter',
  //         url: 'https://pub.dev',
  //         edit: false,
  //         isFresh: false,
  //       ),
  //       Bookmark(
  //         title: 'Boostrap Programming Book',
  //         description: 'Belajar bahasa programing flutter',
  //         url: 'https://boostrap.com',
  //         edit: false,
  //         isFresh: false,
  //       )
  //     ];
  //   });
  // }
}
